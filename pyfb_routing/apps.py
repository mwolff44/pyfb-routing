# -*- coding: utf-8
from django.apps import AppConfig


class PyfbRoutingConfig(AppConfig):
    name = 'pyfb_routing'
