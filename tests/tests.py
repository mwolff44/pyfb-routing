#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client
from pyfb_routing.models import CustomerRoutingGroup, RoutingGroup, PrefixRule, DestinationRule, CountryTypeRule, CountryRule, RegionTypeRule, RegionRule, DefaultRule


def create_customerroutinggroup(**kwargs):
    defaults = {}
    defaults["description"] = "description"
    defaults.update(**kwargs)
    if "customer" not in defaults:
        defaults["customer"] = create_customer()
    if "routinggroup" not in defaults:
        defaults["routinggroup"] = create_routinggroup()
    return CustomerRoutingGroup.objects.create(**defaults)


def create_routinggroup(**kwargs):
    defaults = {}
    defaults["slug"] = "slug"
    defaults["name"] = "name"
    defaults["status"] = "status"
    defaults["status_changed"] = "status_changed"
    defaults["created"] = "created"
    defaults["modified"] = "modified"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return RoutingGroup.objects.create(**defaults)


def create_prefixrule(**kwargs):
    defaults = {}
    defaults["prefix"] = "prefix"
    defaults["destnum_length"] = "destnum_length"
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return PrefixRule.objects.create(**defaults)


def create_destinationrule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "destination" not in defaults:
        defaults["destination"] = create_destination()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return DestinationRule.objects.create(**defaults)


def create_countrytyperule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "country" not in defaults:
        defaults["country"] = create_country()
    if "type" not in defaults:
        defaults["type"] = create_type()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return CountryTypeRule.objects.create(**defaults)


def create_countryrule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "country" not in defaults:
        defaults["country"] = create_country()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return CountryRule.objects.create(**defaults)


def create_regiontyperule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "region" not in defaults:
        defaults["region"] = create_region()
    if "type" not in defaults:
        defaults["type"] = create_type()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return RegionTypeRule.objects.create(**defaults)


def create_regionrule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "region" not in defaults:
        defaults["region"] = create_region()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return RegionRule.objects.create(**defaults)


def create_defaultrule(**kwargs):
    defaults = {}
    defaults["status"] = "status"
    defaults["route_type"] = "route_type"
    defaults["weight"] = "weight"
    defaults["priority"] = "priority"
    defaults.update(**kwargs)
    if "c_route" not in defaults:
        defaults["c_route"] = create_routinggroup()
    if "provider_ratecard" not in defaults:
        defaults["provider_ratecard"] = create_providerratecard()
    if "provider_gateway_list" not in defaults:
        defaults["provider_gateway_list"] = create_providerendpoint()
    return DefaultRule.objects.create(**defaults)


class CustomerRoutingGroupViewTest(TestCase):
    '''
    Tests for CustomerRoutingGroup
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerroutinggroup(self):
        url = reverse('pyfb-routing:pyfb_routing_customerroutinggroup_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerroutinggroup(self):
        url = reverse('pyfb-routing:pyfb_routing_customerroutinggroup_create')
        data = {
            "description": "description",
            "customer": create_customer().pk,
            "routinggroup": create_routinggroup().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerroutinggroup(self):
        customerroutinggroup = create_customerroutinggroup()
        url = reverse('pyfb-routing:pyfb_routing_customerroutinggroup_detail', args=[customerroutinggroup.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerroutinggroup(self):
        customerroutinggroup = create_customerroutinggroup()
        data = {
            "description": "description",
            "customer": create_customer().pk,
            "routinggroup": create_routinggroup().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_customerroutinggroup_update', args=[customerroutinggroup.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RoutingGroupViewTest(TestCase):
    '''
    Tests for RoutingGroup
    '''
    def setUp(self):
        self.client = Client()

    def test_list_routinggroup(self):
        url = reverse('pyfb-routing:pyfb_routing_routinggroup_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_routinggroup(self):
        url = reverse('pyfb-routing:pyfb_routing_routinggroup_create')
        data = {
            "slug": "slug",
            "name": "name",
            "status": "status",
            "status_changed": "status_changed",
            "created": "created",
            "modified": "modified",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_routinggroup(self):
        routinggroup = create_routinggroup()
        url = reverse('pyfb-routing:pyfb_routing_routinggroup_detail', args=[routinggroup.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_routinggroup(self):
        routinggroup = create_routinggroup()
        data = {
            "slug": "slug",
            "name": "name",
            "status": "status",
            "status_changed": "status_changed",
            "created": "created",
            "modified": "modified",
            "description": "description",
        }
        url = reverse('pyfb-routing:pyfb_routing_routinggroup_update', args=[routinggroup.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PrefixRuleViewTest(TestCase):
    '''
    Tests for PrefixRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_prefixrule(self):
        url = reverse('pyfb-routing:pyfb_routing_prefixrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_prefixrule(self):
        url = reverse('pyfb-routing:pyfb_routing_prefixrule_create')
        data = {
            "prefix": "prefix",
            "destnum_length": "destnum_length",
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_prefixrule(self):
        prefixrule = create_prefixrule()
        url = reverse('pyfb-routing:pyfb_routing_prefixrule_detail', args=[prefixrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_prefixrule(self):
        prefixrule = create_prefixrule()
        data = {
            "prefix": "prefix",
            "destnum_length": "destnum_length",
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_prefixrule_update', args=[prefixrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DestinationRuleViewTest(TestCase):
    '''
    Tests for DestinationRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_destinationrule(self):
        url = reverse('pyfb-routing:pyfb_routing_destinationrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_destinationrule(self):
        url = reverse('pyfb-routing:pyfb_routing_destinationrule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "destination": create_destination().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_destinationrule(self):
        destinationrule = create_destinationrule()
        url = reverse('pyfb-routing:pyfb_routing_destinationrule_detail', args=[destinationrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_destinationrule(self):
        destinationrule = create_destinationrule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "destination": create_destination().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_destinationrule_update', args=[destinationrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CountryTypeRuleViewTest(TestCase):
    '''
    Tests for CountryTypeRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_countrytyperule(self):
        url = reverse('pyfb-routing:pyfb_routing_countrytyperule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_countrytyperule(self):
        url = reverse('pyfb-routing:pyfb_routing_countrytyperule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "country": create_country().pk,
            "type": create_type().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_countrytyperule(self):
        countrytyperule = create_countrytyperule()
        url = reverse('pyfb-routing:pyfb_routing_countrytyperule_detail', args=[countrytyperule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_countrytyperule(self):
        countrytyperule = create_countrytyperule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "country": create_country().pk,
            "type": create_type().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_countrytyperule_update', args=[countrytyperule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CountryRuleViewTest(TestCase):
    '''
    Tests for CountryRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_countryrule(self):
        url = reverse('pyfb-routing:pyfb_routing_countryrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_countryrule(self):
        url = reverse('pyfb-routing:pyfb_routing_countryrule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "country": create_country().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_countryrule(self):
        countryrule = create_countryrule()
        url = reverse('pyfb-routing:pyfb_routing_countryrule_detail', args=[countryrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_countryrule(self):
        countryrule = create_countryrule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "country": create_country().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_countryrule_update', args=[countryrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegionTypeRuleViewTest(TestCase):
    '''
    Tests for RegionTypeRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_regiontyperule(self):
        url = reverse('pyfb-routing:pyfb_routing_regiontyperule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_regiontyperule(self):
        url = reverse('pyfb-routing:pyfb_routing_regiontyperule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "region": create_region().pk,
            "type": create_type().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_regiontyperule(self):
        regiontyperule = create_regiontyperule()
        url = reverse('pyfb-routing:pyfb_routing_regiontyperule_detail', args=[regiontyperule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_regiontyperule(self):
        regiontyperule = create_regiontyperule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "region": create_region().pk,
            "type": create_type().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_regiontyperule_update', args=[regiontyperule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RegionRuleViewTest(TestCase):
    '''
    Tests for RegionRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_regionrule(self):
        url = reverse('pyfb-routing:pyfb_routing_regionrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_regionrule(self):
        url = reverse('pyfb-routing:pyfb_routing_regionrule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "region": create_region().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_regionrule(self):
        regionrule = create_regionrule()
        url = reverse('pyfb-routing:pyfb_routing_regionrule_detail', args=[regionrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_regionrule(self):
        regionrule = create_regionrule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "region": create_region().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_regionrule_update', args=[regionrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DefaultRuleViewTest(TestCase):
    '''
    Tests for DefaultRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_defaultrule(self):
        url = reverse('pyfb-routing:pyfb_routing_defaultrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_defaultrule(self):
        url = reverse('pyfb-routing:pyfb_routing_defaultrule_create')
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_defaultrule(self):
        defaultrule = create_defaultrule()
        url = reverse('pyfb-routing:pyfb_routing_defaultrule_detail', args=[defaultrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_defaultrule(self):
        defaultrule = create_defaultrule()
        data = {
            "status": "status",
            "route_type": "route_type",
            "weight": "weight",
            "priority": "priority",
            "c_route": create_routinggroup().pk,
            "provider_ratecard": create_providerratecard().pk,
            "provider_gateway_list": create_providerendpoint().pk,
        }
        url = reverse('pyfb-routing:pyfb_routing_defaultrule_update', args=[defaultrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
