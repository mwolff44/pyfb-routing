.. :changelog:

History
-------

1.0.1 (2020-05-21)
++++++++++++++++++

* update dependencies


1.0.0 (2019-06-22)
++++++++++++++++++

* Admin interface enhancements

0.9.0 (2018-12-05)
++++++++++++++++++

* First release on PyPI.
