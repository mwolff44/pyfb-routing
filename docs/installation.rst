============
Installation
============

At the command line::

    $ easy_install pyfb-routing

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-routing
    $ pip install pyfb-routing
