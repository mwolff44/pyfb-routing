=====
Usage
=====

To use pyfb-routing in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_routing.apps.PyfbRoutingConfig',
        ...
    )

Add pyfb-routing's URL patterns:

.. code-block:: python

    from pyfb_routing import urls as pyfb_routing_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_routing_urls)),
        ...
    ]
